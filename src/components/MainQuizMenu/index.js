import React,{useState} from 'react'
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import icons from '../../assets';

function MainQuizMenu() {

    const {Car,Culture,History,Programming,Tech} = icons

    const [menu, setMenu] = useState([
      {name:'Motoryzacja', class:'Moto', img:Car , alt:'CarIcon'},
      {name:'Kultura', class:'Kul', img:Culture , alt:'CultureIcon'},
      {name:'Technologia', class:'Tech', img:Tech , alt:'TechIcon'},
      {name:'Programowanie', class:'Prog', img:Programming , alt:'ProgrammingIcon'},
      {name:'Historia', class:'His', img:History , alt:'HistoryIcon'},
    ])
  
      return (
          
        <React.Fragment>
                    <div className="Main-quizMenu">
                      <div className="Main-quizMenu-header__container">
                        <h1 className="Main-quizMenu-header">QUIZ</h1>
                        <h2 className="Main-quizMenu-header__h2">5 PYTAŃ/ 5 KATEGORII</h2>
                      </div>
                      <div className='Main-quizMenu-components__container'>
                        {
                          menu.map((el,index) =>{
                            return <div key={el.name} index={index} className={`Main-quizMenu-component ${el.class}`}>
                                      <Link to={`/${el.name}`}>
                                        <div className='Main-quizMenu-component__icon'>
                                          <img src={el.img} alt={el.alt}></img>
                                          <p>{el.name}</p>
                                        </div>
                                      </Link>
                                    </div>
                            })
                        }
                      </div>
                    </div>
        </React.Fragment>
          
      )
}

export default MainQuizMenu
