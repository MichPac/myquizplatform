import React from 'react';
import { DragDropContext,Droppable,Draggable } from 'react-beautiful-dnd';
 

function QuizDnd({handleDragEnd,handleDndAnswer,answers,style,classB}) {
    return (
		<div className='Quiz-DnDAnswer'>
			<div className='Quiz-DnDAnswer__DropContext' style={{backgroundColor:style}}>
			
				<DragDropContext onDragEnd={handleDragEnd}>
					<Droppable droppableId="list">
					
						{provided => (
							<div classNAme="Quiz-DnDAnswer__Dropable-container"  ref={provided.innerRef} {...provided.droppableProps}>
							
							{answers.map((el,index)=>{
								return(
								<div className="Quiz-DnDAnswer__Draggable-container" >	
									<Draggable key={el} draggableId={el} index={index}>
									{provided => (
										<div className="Quiz-DnDAnswer__Draggable-Answer" {...provided.draggableProps} 
										{...provided.dragHandleProps} ref={provided.innerRef}
										>
										{el}
										</div>
									)}
									</Draggable>
								</div>	
								)
							})}
							{provided.placeholder}
							</div>
							) 
						}
					</Droppable>
				</DragDropContext>
			</div>
			<div className="Quiz-DnDAnswer__SubmitBtn-container">	
				<button className={`Quiz-DnDAnswer__SubmitBtn ${classB}`} 
				onClick={handleDndAnswer}
				>Zatwierdź kolejność</button>
			</div>
		</div>
    )
}

export default QuizDnd
