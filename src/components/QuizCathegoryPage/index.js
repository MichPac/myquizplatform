import React from 'react'
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';

function QuizCathegory({cathegory,icon,handleBegin,classB,setBackground}) {
    return (
        <React.Fragment>
                <div className="Quiz-cathegoryPage">
                    <div className="Quiz-cathegoryPage-header__container">
                        <h1>QUIZ</h1>
                        <h2>wybrana kategoria:</h2>
                       <p className='Quiz-cathegoryPage-header__btn close-btn'><Link to='/' onClick={()=>setBackground('')}>X</Link></p>
                    </div>
                    <div className='Quiz-cathegoryPage-img__container'>
                        <img className='Quiz-cathegoryPage-img'
                         src={icon} alt="Motoryzacja"/>
                        <p>{cathegory}</p>
                    </div>
                    <div className='Quiz-cathegoryPage-beginBtn__container'>
                        <button onClick={handleBegin}
                        className={`${classB}`}>Rozpocznij</button>
                    </div>
                </div>
        </React.Fragment>
    )
}

export default QuizCathegory
