import React,{useState,useEffect} from 'react'
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import QuizDnd from '../QuizQueDnd';
import ScorePage from '../Score';

function QuizQue(props) {

    const [quizQue, setQuizQue] = useState(props.quizQuestions);
    const [currentQuestion, setCurrentQuestion] = useState(0);
	const [showScore, setShowScore] = useState(false);
	const [style,setStyle] = useState('');
	const [state,setState] = useState(true);
	const [score, setScore] = useState(0);

	const handleAnswerOptionClick = (answer) => {

		
		setStyle('#a6f2a6');
		if (answer.isCorrect) {
			setScore(score + 1);
		}
		else{
			setState(false);
		}
		setTimeout(nextQuest,500)
	};

	const handleRepeatQuiz = () => {
		setShowScore(false);
		setScore(0)
		props.goBack(false)
	}
	const nextQuest = () =>{
		const nextQuestion = currentQuestion + 1;
		if (nextQuestion < quizQue.length) {
			setCurrentQuestion(nextQuestion);
		} else {
			setShowScore(true);
		}
		setStyle('');
		setState(true);
	}

	// DRAG & DROP functionality

	useEffect(() => {
		if(quizQue[currentQuestion].correctOptions){
			setAnswers(props.quizQuestions[`${currentQuestion}`].answerOptions)
			setCorrectAnswers(props.quizQuestions[`${currentQuestion}`].correctOptions)
		}
		
	}, [currentQuestion])
	

	const [answers,setAnswers] = useState(props.quizQuestions[`${currentQuestion}`].answerOptions)
	const [correctAnswers,setCorrectAnswers] = useState(props.quizQuestions[`${currentQuestion}`].correctOptions)

	const handleDragEnd = (result) => {
		if(!result.destination) return;
		const items = Array.from(answers);
		const [reorderedItem] = items.splice(result.source.index,1);
		items.splice(result.destination.index, 0, reorderedItem);
		setAnswers(items)
		
	}

	const handleDndAnswer = () =>{

		if(JSON.stringify(answers) === JSON.stringify(correctAnswers)){
			setScore(score + 1);
			setStyle('#a6f2a6');
		}
		else{
			setStyle('rgb(252, 193, 205)');
		}
		setTimeout(nextQuest,500)
		
	}


    return (
        <div>
			{showScore  ? (
				<ScorePage 
				classB={props.classB}
				score={score} 
				quizQue={quizQue}
				img={props.img}
				cathegory={props.cathegory}
				setBackground={props.setBackground}
				handleRepeatQuiz={handleRepeatQuiz}
				goBack={props.goBack}
				/>
			) : (
				<React.Fragment>
					<div className="Quiz-Question">
						<div className='Quiz-Question-header__container'>
							<h1 className='Quiz-Question-header'>QUIZ</h1>
							<h2 className='Quiz-Question-header__h2'>wybierz poprawną odpowiedź <span>{currentQuestion + 1}/{quizQue.length}</span></h2>
							<div className="Quiz-Question-header__btn__container">
								<p className='close-btn'><Link to='/' onClick={()=>props.setBackground('')}
								>X</Link></p>
								<p  onClick={()=>props.goBack(false)} className='goback-btn'>
								<span>&lt;</span></p>
							</div>
						</div>
						<div className='Quiz-Question__container'>
						<p className='Quiz-Question__question'>{quizQue[currentQuestion].questionText}</p>
						</div>


						{!quizQue[currentQuestion].correctOptions ? 
							<div className='Quiz-Answer'>
							{quizQue[currentQuestion].answerOptions.map((answerOption,index) => (
							<button className="Quiz-Answer__btn"
							key={answerOption.answetText} index={index}  
							style={{backgroundColor: answerOption.isCorrect ? style  :
								 !answerOption.isCorrect  && state ?  '': 'rgb(252, 193, 205)' }}
								 className={props.classB}
							onClick={() => handleAnswerOptionClick(answerOption)}
							>{answerOption.answetText}</button>
							))}
							</div>:

							<QuizDnd 
							classB={props.classB}
							style={style}
							handleDragEnd={handleDragEnd}
							handleDndAnswer={handleDndAnswer}
							answers={answers}
							/>
							
						}
						
					</div>
					
				</React.Fragment>
			)}
		</div>
    )
}

export default QuizQue;


