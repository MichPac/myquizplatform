import React,{useEffect,useState} from 'react'
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import icons from '../../assets/index'

function CathegoryMenu({cathegory,goBack,setBackground,classB}) {

  
  const {Car,Culture,History,Programming,Tech} = icons

  const [sideMenu, setSideMenu] = useState([
      {name:'Motoryzacja', img:Car , alt:'CarIcon'},
      {name:'Kultura', img:Culture , alt:'CultureIcon'},
      {name:'Historia', img:History , alt:'HistoryIcon'},
      {name:'Programowanie', img:Programming , alt:'ProgrammingIcon'},
      {name:'Technologia', img:Tech , alt:'TechIcon'},
  ])


  useEffect(() => {
    const corSideMenu = [...sideMenu].filter(el => el.name !== cathegory)
    setSideMenu(corSideMenu)
  }, [])



    return (
        <div className="SubMenu">
            <p className="SubMenu__p">Wybierz inną kategorię:</p>
            <div className="SubMenu__components-container">
                    {
                      sideMenu.map((el,index) =>{
                        return  <React.Fragment>
                                    <div className="SubMenu-component"
                                    index={index} key={el.name}>
                                      <div className={`SubMenu-component__cathegory-container ${classB}`}>
                                        <Link to={`/${el.name}`} onClick={()=>{setBackground(`${el.img}`);goBack()}}>
                                            <img src={el.img} alt={el.alt}></img>
                                            <p>{el.name}</p>
                                            </Link>
                                        </div>
                                  
                                    </div>
                                </React.Fragment>
                      })

                    }
              </div>
        </div>
    )
}




export default CathegoryMenu
