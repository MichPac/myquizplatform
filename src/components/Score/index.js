import React from 'react'
import QuizScoreMenu from '../QuizScoreMenu';
import {BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';


function ScorePage({classB,cathegory,score,quizQue,img,setBackground,handleRepeatQuiz,goBack}) {

    return (
        <div className="Quiz-Score">
					<div className="Quiz-Score__mainWrapper">
						<div className="Quiz-Score__components-container">
						<div>
								<h1 className='Quiz-Question__header'>QUIZ</h1>
								<h2>Twój wynik to {score} na {quizQue.length}</h2>
								<div className='Quiz_icon'>
								<img src={img} alt="Motoryzacja"/>
								<p>{cathegory}</p>
						</div>
						<div className="Quiz-Score__btn-container">
								<p className='close-btn'><Link to='/' onClick={()=>setBackground('')}>X</Link></p>
								<p  onClick={()=>goBack(false)} className='goback-btn'><span>&lt;</span></p>
							</div>
								<p className="Quiz-Score__answerBtn-container"><button className={classB} 
								onClick={handleRepeatQuiz}>Powtórz quiz</button></p>
							</div>
						</div>
						<div className="Quiz-Score__SubMenu-container">
							
							
							<QuizScoreMenu classB={classB}
							setBackground={setBackground}
							cathegory={cathegory} 
							goBack={goBack}
							/>
						</div> 
					</div>
		</div>
    )
}

export default ScorePage
