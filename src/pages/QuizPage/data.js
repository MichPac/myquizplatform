const motoryzacja = [

    {
        id:'q1',
        questionText: 'Uszereguj samochody w kolejność od najstarszego do najnowszego:',
        answerOptions:['Rolls-Royce SilverSpirit','abarth 595','ferrari 288 GTO','Ford T'],
        correctOptions:['Ford T','ferrari 288 GTO','Rolls-Royce SilverSpirit','abarth 595'],

    },
    // {
    //     id:'q2',
    //     questionText: 'Jaką nazwę ma słynny program telewizyjny, w którym występował Jeremy Clarkson?',
    //     answerOptions:[
    //         {answetText: 'Auto Gear', isCorrect: false},
    //         {answetText: 'Blue Gear', isCorrect: false},
    //         {answetText: '5th Gear', isCorrect: false},
    //         {answetText: 'Top Gear', isCorrect: true},
    //     ]
    // } ,
    // {
    //     id:'q3',
    //     questionText: 'Jaki samochód ma obecnie tytuł najszybszego samochodu na świecie?',
    //     answerOptions:[
    //         {answetText: 'Bugatti Chiron', isCorrect: false},
    //         {answetText: 'Koenigsegg Agera RS', isCorrect: false},
    //         {answetText: 'Bugatti Veyron', isCorrect: false},
    //         {answetText: 'SSC Tuatara', isCorrect: true},
    //     ]
    // } ,
    // {
    //     id:'q4',
    //     questionText: 'Uszereguj samochody od najszybszego do najwolniejszego:',
    //     answerOptions:['Volksvagen Golf','Porshe 918','Mercedes a45','Lamborgini Huracan'],
    //     correctOptions:['Volksvagen Golf','Mercedes a45','Lamborgini Huracan','Porshe 918'],
    // },
    // {
    //     id:'q5',
    //     questionText: 'Która marka oprócz samochodów produkuje również kiełbasy i keczup?',
    //     answerOptions:[
    //         {answetText: 'Volksvagen', isCorrect: true},
    //         {answetText: 'Tesla', isCorrect: false },
    //         {answetText: 'Toyota', isCorrect: false},
    //         {answetText: 'Mercedes', isCorrect: false},
    //     ]
    // } 

]

const kultura = [

    {
        id:'q1',
        questionText: 'Uszereguj filmy zaczynając od najmniej do najbardziej utytułowanego pod względem zdobytych Oskarów:',
        answerOptions:['Avatar','Ratatuj','Titanic','Lot nad kukułczym gniazdem'],
        correctOptions:['Ratatuj','Avatar','Lot nad kukułczym gniazdem','Titanic'],
    },
    {
        id:'q2',
        questionText: 'Który z wymienionych teatrów operowych ma największą na świecie widownię?',
        answerOptions:[
            {answetText: 'Teatro alla Scala w Mediolanie', isCorrect: false},
            {answetText: 'Sydney Opera House', isCorrect: false},
            {answetText: 'Metropolitan Opera House w Nowym Jorku', isCorrect: true},
            {answetText: 'Royal Opera House w Londynie', isCorrect: false},
        ]
    } ,
    {
        id:'q3',
        questionText: 'Kto jest autorem obrazu pt. "Krzyk"?',
        answerOptions:[
            {answetText: 'Jan Matejko', isCorrect: false},
            {answetText: 'Edvard Munch', isCorrect: true},
            {answetText: 'Salvador Dali', isCorrect: false},
            {answetText: 'Pablo Picasso', isCorrect: false},
        ]
    } ,
    {
        id:'q4',
        questionText: '"Dama z ..." to słynny obraz Leonardo Da Vinci',
        answerOptions:[
            {answetText: 'Łysiczką', isCorrect: false},
            {answetText: 'Fretką', isCorrect: false},
            {answetText: 'Tchórzem zwyczajnym', isCorrect: false},
            {answetText: 'Gronostajem', isCorrect: true},
        ]
    } ,
    {
        id:'q5',
        questionText: 'Jaki tytuł nosi ksiązka Olgi Tokarczuk, za którą otrzymała Nagordę Nobla?',
        answerOptions:[
            {answetText: 'Księgi Jakubowe', isCorrect: false},
            {answetText: 'Bieguny', isCorrect: true},
            {answetText: 'Zagubiona Dusza', isCorrect: false},
            {answetText: 'Ostatnie historie', isCorrect: false},
            
        ]
    },

    

]

const historia = [

    {
        id:'q1',
        questionText: 'Uszereguj chronologicznie wydarzenia zaczynając od najstarszego:',
        answerOptions:['Bitwa pod Grunwaldem','Koronacja Augusta Poniatowskiego','Schizma Wschodnia','I rozbiór Polski'],
        correctOptions:['Schizma Wschodnia','Bitwa pod Grunwaldem','I rozbiór Polski','Koronacja Augusta Poniatowskiego'],

    },
    {
        id:'q2',
        questionText: 'Kiedy ropoczeła się pierwsza wojna światowa?',
        answerOptions:[
            {answetText: '1939', isCorrect: false},
            {answetText: '1917', isCorrect: false},
            {answetText: '1918', isCorrect: false},
            {answetText: '1915', isCorrect: true},
        ]
    } ,
    {
        id:'q3',
        questionText: 'Uszereguj po kolei dynastie panujące w Chinach zaczynając od najstarszej:',
        answerOptions:['Dynastia Qin','Dynastia Shang','Dynastia Ming','Dynastia Zhou'],
        correctOptions:['Dynastia Shang','Dynastia Zhou','Dynastia Qin','Dynastia Ming'],

    },
    {
        id:'q4',
        questionText: 'Jaki jest rzymski odpowiednik starozytnego boga greckiego Aresa?',
        answerOptions:[
            {answetText: 'Neptun', isCorrect: false},
            {answetText: 'Mars', isCorrect: true},
            {answetText: 'Jupiter', isCorrect: false},
            {answetText: 'Pluton', isCorrect: false},
        ]
    },
    {
        id:'q5',
        questionText: 'Którego roku zakończyła się 2. wojna światowa?',
        answerOptions:[
            {answetText: '1955', isCorrect: false},
            {answetText: '1939', isCorrect: false},
            {answetText: '1945', isCorrect: true},
            {answetText: '1920', isCorrect: false},
        ]
    }
    

]

const programowanie = [
    {
        id:'q1',
        questionText: 'Uszereguj w kolejności prawidłowego uzywania znaczniki HTML potrzebne przy tworzeniu tabeli:',
        answerOptions:['<tbody>','<td>','<tr>','<table>'],
        correctOptions:['<table>','<tbody>','<tr>','<td>'],
    } ,
    {
        id:'q2',
        questionText: 'Który wybór nie jest poprawną wartością dla właściwości font-style?',
        answerOptions:[
            {answetText: 'none', isCorrect: true},
            {answetText: 'italic', isCorrect: false},
            {answetText: 'normal', isCorrect: false},
            {answetText: 'oblique', isCorrect: false},
        ]
    } ,
    {
        id:'q3',
        questionText: 'W którym roku został udostępniony React.js?',
        answerOptions:[
            {answetText: '2013', isCorrect: true},
            {answetText: '2015', isCorrect: false},
            {answetText: '2011', isCorrect: false},
            {answetText: '2014', isCorrect: false},
        ]
    } ,
    {
        id:'q4',
        questionText: 'Która odpowiedź to niepoprawny zapis definujący funkcje strzałkową zwracającą pusty objekt?',
        answerOptions:[
            {answetText: '() => {return {}; }', isCorrect: false},
            {answetText: '() => {}', isCorrect: true},
            {answetText: '() => (({}))', isCorrect: false},
            {answetText: '() => ({})', isCorrect: false},
        ]
    }, 
    {
        id:'q5',
        questionText: 'Które nazwy odpowiadają danym wartościom: font-weight: 400; font-weight: 700?',
        answerOptions:[
            {answetText: 'font-weight: bold; font-weight: normal', isCorrect: false},
            {answetText: 'font-weight: light; font-weight: normal', isCorrect: false},
            {answetText: 'font-weight: normal; font-weight: bold', isCorrect: true},
            {answetText: 'font-weight: normal; font-weight: bolder', isCorrect: false},
        ]
    }

    

]

const technologia = [

    {
        id:'q1',
        questionText: 'Podaj kolejność spółek technologicznych pod względem wartości rynkowej zaczynając od największej:',
        answerOptions:['Apple','Google','Tencent','Microsoft'],
        correctOptions:['Apple','Microsoft','Google','Tencent'],
    } ,
    {
        id:'q2',
        questionText: 'Jaka firma na początku nazywała się X.com?',
        answerOptions:[
            {answetText: 'Revolut', isCorrect: false},
            {answetText: 'BlueCash', isCorrect: false},
            {answetText: 'PayPal', isCorrect: true},
            {answetText: 'Tencent', isCorrect: false},
        ]
    } ,
    {
        id:'q3',
        questionText: 'Gdzie mieści się główna siedziba Apple?',
        answerOptions:[
            {answetText: 'San Francisco', isCorrect: true},
            {answetText: 'Nowy York', isCorrect: false},
            {answetText: 'Austin', isCorrect: false},
            {answetText: 'Los Angeles', isCorrect: false},
        ]
    } ,
    {
        id:'q4',
        questionText: 'Co zwykle oznacza skrót komputerowy "OS"? ',
        answerOptions:[
            {answetText: 'Order of Significance', isCorrect: false},
            {answetText: 'Open Software', isCorrect: false},
            {answetText: 'Optical Sensor', isCorrect: false},
            {answetText: 'Operating System', isCorrect: true},
        ]
    },
    {
        id:'q5',
        questionText: 'Który język programownia powstał jako pierwszy? Uszereguj zaczynając od najstarszego:',
        answerOptions:['COBOL','C++','Fortran','BASIC'],
        correctOptions:['Fortran','COBOL','BASIC','C++'],
    } ,

    

]

const questions = {motoryzacja,kultura,historia,programowanie,technologia};

export default questions
