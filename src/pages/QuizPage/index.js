import React,{useState,useEffect} from 'react';
import questions from './data';
import QuizQue from '../../components/Quiz';
import QuizCathegory from '../../components/QuizCathegoryPage';


function QuizPage({cathegory,icon,setBackground,setBtnStyle,btnStyle,classB}) {

    const {motoryzacja,kultura,historia,programowanie,technologia} = questions;

    const questionsArray = [
        {name:'Motoryzacja',questions:motoryzacja},
        {name:'Kultura',questions:kultura},
        {name:'Historia',questions:historia},
        {name:'Programowanie',questions:programowanie},
        {name:'Technologia',questions:technologia},
    ];

    const [quizQue, setQuizQue] = useState();
    const [state,setState] = useState(true);

    useEffect(() => {
        if(cathegory){
            setBackground(icon)
            setBtnStyle(btnStyle)
        }
    }, [cathegory])

    function handleBegin(){
        for(let i=0;i < questionsArray.length;i++){
            if(cathegory===questionsArray[i].name){
                setQuizQue(questionsArray[i].questions);
                setState(true);
            }
        }
    }

    return (
        <React.Fragment>
            <div>
            {quizQue && state ?  
                <QuizQue
                classB={classB}
                btnStyle={btnStyle} 
                setBtnStyle={setBtnStyle}
                img={icon} 
                quizQuestions={quizQue} 
                goBack={setState} 
                cathegory={cathegory} 
                setBackground={setBackground}
                />
                :
                <QuizCathegory 
                classB={classB}
                btnStyle={btnStyle} 
                setBtnStyle={setBtnStyle}
                cathegory={cathegory} 
                icon={icon} 
                handleBegin={handleBegin} 
                setBackground={setBackground}
                />
            }
                
            </div>
        </React.Fragment>
    )
}

export default QuizPage
