import './App.css';
import React,{useState} from 'react'
import Homepage from './pages/Homepage'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import QuizPage from './pages/QuizPage';
import icons from './assets';
import './styles/themes/default/theme.scss';

function App() {

  const {Car,Culture,History,Programming,Tech} = icons;
  const[backgroundC,setBackground] = useState();
  const [btnStyle,setBtnStyle] = useState('');

  const RouteArray = [
    {path:'/motoryzacja',cathegory:'Motoryzacja',classB:'Moto', icon:Car,color:'#5741d9'},
    {path:'/kultura',cathegory:'Kultura',classB:'Kul', icon:Culture,color:'#dca3c8'},
    {path:'/historia',cathegory:'Historia',classB:'His', icon:History,color:'#0385e6'},
    {path:'/programowanie',cathegory:'Programowanie',classB:'Prog', icon:Programming,color:'#ac9ef9'},
    {path:'/technologia',cathegory:'Technologia',classB:'Tech', icon:Tech,color:'#8144c2'}
  ]

  return (
    <div className="App">
      <div className='App_container'>
      <div className="App-container__components"> 
      <React.Fragment>
            <Router>
              <Switch>
                <Route exact path='/' component={Homepage}/>
                {
                  RouteArray.map((el,index)=>(
                    <Route path={el.path}>
                      <QuizPage 
                      classB={el.classB}
                      setBtnStyle={setBtnStyle}
                      btnStyle={btnStyle}
                      key={el.cathegory} 
                      index={index} 
                      cathegory={el.cathegory}
                      setBackground={setBackground} 
                      classB={el.classB} icon={el.icon}/>
                    </Route>
                  ))
                }
              </Switch>
            </Router>
      </React.Fragment>
      </div>
      <div className="App-container__backgroundImgs">
        <img className='App-backgroundImg bPic1' src={backgroundC} />
        <img className='App-backgroundImg bPic2' src={backgroundC} />
        <img className='App-backgroundImg bPic3' src={backgroundC} />
        <img className='App-backgroundImg bPic4' src={backgroundC} />
        <img className='App-backgroundImg bPic5' src={backgroundC} />
        <img className='App-backgroundImg bPic6' src={backgroundC} />
      </div>
      </div>
    </div>
  );
}

export default App;
