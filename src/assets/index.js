import Car from './icons/carIcon.png'
import Culture from './icons/cultureIcon.png'
import History from './icons/historyIcon.png'
import Programming from './icons/progIcon.png'
import Tech from './icons/techIcon.png'


const icons = {
    Car,Culture,History,Programming,Tech
}

export default icons